
<div class="post-container">
    <form class="form-post-create">
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1">Title</span>
            </div>
            <input type="text" class="form-control" name="title" placeholder="Post title" aria-label="Post title" aria-describedby="basic-addon1">
        </div>

        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text">Text</span>
            </div>
            <textarea class="form-control" name="text" aria-label="With textarea"></textarea>
        </div>

        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>

<style>
    .post-container {
        margin: 0 auto;
        margin-top: 100px;
        width: 50%;
    }
</style>

<script>
    $(".form-post-create").submit((event) => {

        let formData = {};
        for(let key in event.target) {
            if(event.target[key] != null && event.target[key].localName == "input") {
                formData[event.target[key].name] = event.target[key].value
            }
            if(event.target[key] != null && event.target[key].localName == "textarea") {
                formData[event.target[key].name] = event.target[key].value
            }
        }

        sendAjax("POST", "/core/requests/ajax/createPost.php", formData, (response) => {
            window.location.href = "/post/"+response["postID"];
        })

        event.preventDefault();
    })
</script>