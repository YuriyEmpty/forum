<div class="posts-container">
    <?foreach ($posts as $key => $value):?>
        <div class="card">
            <img src="/public/images/postBackground.jpg" class="card-img-top" alt="someone">
            <div class="card-body">
                <h5 class="card-title"><?=$value["title"]?></h5>
                <p class="card-text"><?=substr($value["text"], 0, 30)."..."?></p>
                <a href="/post/<?=$value["id"]?>" class="btn btn-primary">View detail</a>
            </div>
        </div>
    <?endforeach;?>
</div>

<style>
    .card {
        margin:5px;
        width: calc(20% - 10px) !important;
        float: left;
    }
</style>



