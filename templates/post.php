<div class="card" data-id="<?=$post["id"]?>">
  <div class="card-header">
      <div>Published by: <span><?=$post["owner"]["email"]?></span></div>
      <div>Is resolved: <span id="resolved-status"><?=$post["is_resolved"]?></span></div>
  </div>
  <div class="card-body">
    <h5 class="card-title"><?=$post["title"]?></h5>
    <p class="card-text"><?=$post["text"]?></p>
    <a href="/" class="btn btn-primary">Return to main</a>
    <?if($post["is_resolved"] != "Yes" && $post["owner_id"] == \Core\Classes\User::getCurrentUser()["id"]): ?>
        <button type="button" id="btn-resolved" class="btn btn-primary">Mark as decided</button>
    <?endif;?>

      <?if($post["is_resolved"] != "Yes"):?>
          <div class="input-group" id="block-comment">
              <div class="input-group-prepend">
                  <span class="input-group-text">Comment</span>
              </div>
              <textarea class="form-control" id="text-comment" aria-label="With textarea"></textarea>
          </div>
          <button class="btn btn-primary" id="btn-comment" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">Add comment</button>
      <?endif;?>
  </div>

</div>
<?foreach ($post["commentaries"] as $key => $value):?>
    <div class="collapse multi-collapse show" id="multiCollapseExample1">
        <div class="card card-body">
            <span class="user-email"><?=$value["owner"]["email"]?></span>
            <span class="comment-text"><?=$value["text"]?></span>
        </div>
    </div>
<?endforeach;?>

<style>
    .user-email {
        font-size: 20px;
    }
    .comment-text {
        margin-left: 20px;
    }
    .input-group {
        margin-top: 10px;
        margin-bottom: 10px;
    }
    #btn-comment {
        float: right;
    }
</style>

<script>
    let buttonResolved = document.getElementById("btn-resolved");
    let buttonComment = document.getElementById("btn-comment");
    let blockComment = document.getElementById("block-comment");

    if(buttonResolved) {
        buttonResolved.addEventListener("click", (event) => {
            let id = document.getElementsByClassName("card")[0];
            sendAjax("POST", "/core/requests/ajax/changePost.php", {id: id.getAttribute("data-id")}, (response) => {
                let status = document.getElementById("resolved-status");
                status.innerHTML = "Yes";
                buttonResolved.outerHTML = "";
                buttonComment.outerHTML = "";
                blockComment.outerHTML = "";
            });
        });
    }

    if(buttonComment) {
        buttonComment.addEventListener("click", (event) => {
            let commentArea = document.getElementById("text-comment");
            if(!commentArea.value) return;
            let id = document.getElementsByClassName("card")[0];
            sendAjax("POST", "/core/requests/ajax/setComment.php", {id: id.getAttribute("data-id"), data: commentArea.value}, (response) => {
                window.location.reload();
            });

        });
    }
</script>
