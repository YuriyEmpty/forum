<!DOCTYPE html>
<html lang="<?=$language?>">
<head>
    <meta charset="UTF-8">
    <title><?=$title?></title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="/public/js/app.js"></script>
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="/">Forum</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <?if(\Core\Classes\User::isAuthorize()):?>
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="/create-post">Create post <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="logout" href="#"><?="Logout: ".\Core\Classes\User::getCurrentUser()["email"]?></a>
                    </li>
                </ul>
                <script>
                    let logout = document.getElementById("logout");

                    logout.addEventListener("click", () => {
                        sendAjax("POST", "/core/requests/ajax/logout.php", {}, () => {
                            window.location.reload();
                        })
                    })
                </script>
            <?else:?>
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="#"><?="Welcome Guest"?></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/login"><?="Login/Registration"?></a>
                    </li>
                </ul>
            <?endif;?>
            <form class="form-inline my-2 my-lg-0" id="search-form">
                <input class="form-control mr-sm-2" id="search-input" type="search" placeholder="Search" aria-label="Search">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                <div class="dropdown-menu dropdown-menu-lg-right" id="search-dropdown">

                </div>
            </form>
        </div>
    </nav>

    <?\Core\Controller::includeTemplate($template, get_defined_vars()["variables"]);?>

    <style>
        .dropdown-item {
            cursor: pointer;
        }
    </style>

    <script>

        $("#search-form").submit((event) => {
            let input = document.getElementById("search-input");
            if(input.value) {

                sendAjax("POST", "/core/requests/ajax/searchForm.php", {data: input.value}, (response) => {
                    if(response.data == undefined) return;

                    let drowpdown = document.getElementById("search-dropdown");
                    drowpdown.innerHTML = "";
                    for(let key in response.data) {
                        let elementLink = document.createElement("a");
                        elementLink.classList.add("dropdown-item");
                        elementLink.setAttribute("href", "/post/"+response.data[key].id);
                        elementLink.innerHTML = response.data[key].title;

                        drowpdown.appendChild(elementLink);
                    }

                    var documentClickEvent = () => {
                        drowpdown.classList.remove("show");
                        input.value = "";
                        document.removeEventListener("click", documentClickEvent);
                    }

                        document.addEventListener("click", documentClickEvent)
                    drowpdown.classList.add("show");
                })

            }

            event.preventDefault();
        });

    </script>

</body>
</html>

