<div class="text-center">
    <form class="form-register">
        <h1 class="h3 mb-3 font-weight-normal">Registration</h1>
        <label for="inputEmail" class="sr-only">Email address</label>
        <input type="email" id="inputEmail" class="form-control" placeholder="Email address" name="email" required="" autofocus="">
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" id="inputPassword" class="form-control" placeholder="Password" name="password" required="">
        <div class="checkbox mb-3"></div>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Register</button>
        <a class="btn btn-primary btn-block" href="/login" role="button">Authorize</a>
    </form>
</div>

<style>
    .form-register {
        width: 100%;
        max-width: 330px;
        padding: 15px;
        margin: auto;
        margin-top: 200px;
    }

    .text-center {
        margin: auto;
        max-width: 330px;
    }
</style>

<script>
    $(".form-register").submit((event) => {

        let formData = {};
        for(let key in event.target) {
            if(event.target[key] != null && event.target[key].localName == "input") {
                formData[event.target[key].name] = event.target[key].value
            }
        }

        sendAjax("POST", "/core/requests/ajax/registration.php", formData, () => {
            window.location.href = "/";
        })

        event.preventDefault();
    })

</script>