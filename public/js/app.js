function sendAjax(type = "POST", url = "", data = {}, successCallback) {
    $.ajax({
        type: type,
        url: url,
        data: {data: JSON.stringify(data)},
        success: (response) => {
            response = JSON.parse(response);

            if(response.type == "success") {
                console.log("success")
                successCallback(response);
            }
            if(response.type == "error") {
                console.log("error")
            }

        }
    });
}