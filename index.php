<?php
/**
 * Create by: Yuriy Empty
 * Date: 01.03.2019
 * Time: 19:31
 */

/*
|--------------------------------------------------------------------------
| Register The Auto Loader
|--------------------------------------------------------------------------
*/
require_once __DIR__.'/vendor/autoload.php';

use Core\Router;

$router = new Router();
$router->dispatch();



