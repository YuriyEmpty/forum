##Forum MVC app on native php


**Step 1:** Clone project - git clone https://bitbucket.org/YuriyEmpty/forum.git

**Step 2:** import mysql /databaseImport/forum.sql

**Step 3:** install composer dependencies - composer update, composer install

**Step 4:** run project using apache server

**if need change database settings, use /Core/Database/config.php (default database name - "forum")**

