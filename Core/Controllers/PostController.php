<?php
/**
 * Create by: Yuriy Empty
 * Date: 03.03.2019
 * Time: 12:52
 */

namespace Core\Controllers;


use Core\Controller;
use Core\Database\RequestsManager\RequestManager;

class PostController extends Controller {

    public function __construct() {}

    public function show() {

        $database = new RequestManager();

        $path = explode("/", $_REQUEST["route"]);
        $postID = $path[count($path) - 1];

        $postData = $database->getPost($postID);
        if(empty($postData)) {
            header("Location: / ");
            exit();
        }

        if($postData["is_resolved"] == 0) {
            $postData["is_resolved"] = "No";
        } else {
            $postData["is_resolved"] = "Yes";
        }

        $postData["owner"] = $database->getUserByID($postData["owner_id"]);
        $postData["commentaries"] = $database->getCommentaries($postData["id"])->getArray();

        foreach ($postData["commentaries"] as $key => $value) {
            $owner = $database->getUserByID($value["owner_id"]);
            $postData["commentaries"][$key]["owner"] = $owner;
        }

        /*echo "<pre>";
        var_dump($postData);
        echo "</pre>";*/



        $array = [
            "language" => "en",
            "title" => "Post: ".$postData["title"],
            "post" => $postData
        ];

        return $array;
    }

}