<?php
/**
 * Create by: Yuriy Empty
 * Date: 02.03.2019
 * Time: 16:25
 */

namespace Core\Controllers;


use Core\Controller;
use Core\Database\RequestsManager\RequestManager;

class IndexController extends Controller {

    public function __construct() {

    }

    public function show() {

        $database = new RequestManager();

        $posts = $database->getAllPosts();

        $array = [
            "language" => "en",
            "title" => "IndexController",
            "posts" => $posts
        ];

        return $array;
    }


}