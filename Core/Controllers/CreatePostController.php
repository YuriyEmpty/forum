<?php
/**
 * Create by: Yuriy Empty
 * Date: 03.03.2019
 * Time: 15:21
 */

namespace Core\Controllers;


use Core\Controller;

class CreatePostController extends Controller {

    public function __construct() {

    }

    public function show() {

        $array = [
            "language" => "en",
            "title" => "Create new post",
        ];

        return $array;
    }


}