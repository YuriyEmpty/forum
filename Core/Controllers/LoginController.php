<?php
/**
 * Create by: Yuriy Empty
 * Date: 03.03.2019
 * Time: 01:17
 */

namespace Core\Controllers;


use Core\Controller;

class LoginController extends Controller {

    public function __construct() {

    }

    public function show() {

        $array = [
            "language" => "en",
            "title" => "Login"
        ];

        return $array;
    }
}