<?php
/**
 * Create by: Yuriy Empty
 * Date: 02.03.2019
 * Time: 18:33
 */

namespace Core\Controllers;


use Core\Controller;

class RegistrationController extends Controller {

    public function __construct() {}

    public function show() {

        $array = [
            "language" => "en",
            "title" => "Registration"
        ];

        return $array;
    }

}