<?php

require_once $_SERVER["DOCUMENT_ROOT"].'/vendor/autoload.php';
$commentData = json_decode($_POST["data"], true);

$database = new Core\Database\RequestsManager\RequestManager();
$ownerID = \Core\Classes\User::getCurrentUser()["id"];

$currentPost = $database->getPost($commentData["id"]);

if($currentPost["is_resolved"] == 0) {
    $isSet = $database->setComment($commentData["id"],$ownerID , $commentData["data"]);
}

if($isSet) {
    $response = ["type" => "success"];
} else {
    $response = ["type" => "error"];
}


echo json_encode($response);