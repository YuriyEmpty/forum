<?php

require_once $_SERVER["DOCUMENT_ROOT"].'/vendor/autoload.php';

$postData = json_decode($_POST["data"], true);

$postData["owner"] = \Core\Classes\User::getCurrentUser();

$database = new \Core\Database\RequestsManager\RequestManager();
$postID = $database->setPost($postData);

if($postID) {
    $response = ["type" => "success",
                 "postID" => $postID];
} else {
    $response = ["type" => "error"];
}

echo json_encode($response);