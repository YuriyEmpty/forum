<?php

require_once $_SERVER["DOCUMENT_ROOT"].'/vendor/autoload.php';

$postData = json_decode($_POST["data"], true);

$user = \Core\Classes\User::getCurrentUser();

$response = ["type" => "error"];
if($user) {

    $database = new \Core\Database\RequestsManager\RequestManager();

    $post = $database->getPost($postData["id"]);

    if($user["id"] == $post["owner_id"]) {

        $result = $database->updatePost($post["id"], "is_resolved","1");
        if($result) $response["type"] = "success";

    }
}

echo json_encode($response);
