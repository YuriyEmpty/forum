<?php

require_once $_SERVER["DOCUMENT_ROOT"].'/vendor/autoload.php';

$userData = json_decode($_POST["data"], true);

$isLogout = \Core\Classes\User::logout();

if($isLogout) {
    $response = ["type" => "success"];
} else {
    $response = ["type" => "error"];
}

echo json_encode($response);