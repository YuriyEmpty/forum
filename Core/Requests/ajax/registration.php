<?php

require_once $_SERVER["DOCUMENT_ROOT"].'/vendor/autoload.php';

$userData = json_decode($_POST["data"], true);

$database = new Core\Database\RequestsManager\RequestManager();

$isRegistered = $database->registerUser($userData["email"], $userData["password"]);

if($isRegistered) {
    $response = ["type" => "success"];

    $user = new \Core\Classes\User();
    $user->authorize($userData["email"], $userData["password"]);

} else {
    $response = ["type" => "error"];
}

echo json_encode($response);