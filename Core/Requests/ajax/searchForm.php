<?php
require_once $_SERVER["DOCUMENT_ROOT"].'/vendor/autoload.php';

$searchData = json_decode($_POST["data"], true);


$database = new Core\Database\RequestsManager\RequestManager();

$posts = $database->getAllPosts();

$foundedPosts = new \Core\Classes\Model\Model();
foreach($posts as $key => $value) {

    $searchString = $value["title"].$value["text"];
    $isFounded = strripos($searchString, $searchData["data"]);
    if($isFounded !== false) $foundedPosts->set($value["id"], ["id" => $value["id"], "title" => $value["title"]]);
}

if($foundedPosts->length() > 0) {
    $response = ["type" => "success",
                 "data" => $foundedPosts->getArray()];
} else {
    $response = ["type" => "error"];
}

echo json_encode($response);