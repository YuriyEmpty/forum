<?php
/**
 * Create by: Yuriy Empty
 * Date: 02.03.2019
 * Time: 23:13
 */

namespace Core\Database\RequestsManager;

use Core\Classes\Model\Model;
use Core\Database\Connection;

class RequestManager extends Connection {

    /**
     * @param $email
     * @param $password
     * @return bool|\mysqli_result
     */
    public function registerUser($email, $password) {
        $user = $this->getUserByEmail($email);

        if($user) return false;

        $this->connect();

        $query = "INSERT INTO users (email, password) VALUES ('$email', '$password');";
        $result = mysqli_query($this->connection, $query) or die("Ошибка запроса " . mysqli_error($this->connection));

        $this->close();

        return $result;
    }

    /**
     * @param $email
     * @return bool|\mysqli_result
     */
    public function getUserByEmail($email) {

        $this->connect();

        $user = false;
        $query = "SELECT * FROM users WHERE email = '$email';";
        $result = mysqli_query($this->connection, $query) or die("Ошибка запроса " . mysqli_error($this->connection));

        if($result && mysqli_num_rows($result) > 0) {
            foreach ($result as $key => $value) {
                $user = $value;
            }
        }

        $this->close();

        return $user;
    }

    public function getUserByID($id) {

        $this->connect();

        $user = false;
        $query = "SELECT * FROM users WHERE id = '$id';";
        $result = mysqli_query($this->connection, $query) or die("Ошибка запроса " . mysqli_error($this->connection));

        if($result && mysqli_num_rows($result) > 0) {
            foreach ($result as $key => $value) {
                $user = $value;
            }
        }

        $this->close();

        return $user;
    }

    /**
     * @param $postData
     * @return bool|\mysqli_result
     */
    public function setPost($postData) {

        $this->connect();

        $owner_id = $postData["owner"]["id"];
        $title = $postData["title"];
        $text = $postData["text"];

        $query = "INSERT INTO posts (owner_id, title, text) VALUES ('$owner_id', '$title', '$text');";
        $result = mysqli_query($this->connection, $query) or die("Ошибка запроса " . mysqli_error($this->connection));

        $result = $this->connection->insert_id;

        $this->close();
        return $result;
    }

    public function getPost($id) {

        $this->connect();
        $query = "SELECT * FROM posts WHERE id = '$id';";
        $result = mysqli_query($this->connection, $query) or die("Ошибка запроса " . mysqli_error($this->connection));

        $post = [];

        if($result && mysqli_num_rows($result) > 0) {
            foreach ($result as $key => $value) {
                $post = $value;
            }
        }

        $this->close();

        return $post;
    }

    public function updatePost($id, $column, $value) {

        $this->connect();
        $query = "UPDATE posts SET $column = $value WHERE id = '$id';";
        $result = mysqli_query($this->connection, $query) or die("Ошибка запроса " . mysqli_error($this->connection));

        $this->close();

        return $result;
    }

    public function setComment($postID, $ownerID, $text) {
        $this->connect();

        $query = "INSERT INTO commentaries (post_id, owner_id, text) VALUES ('$postID', '$ownerID', '$text');";
        $result = mysqli_query($this->connection, $query) or die("Ошибка запроса " . mysqli_error($this->connection));

        $this->close();
        return $result;
    }

    public function getCommentaries($postID) {

        $commentariesModel = new Model();

        $this->connect();
        $query = "SELECT * FROM commentaries WHERE post_id = '$postID';";
        $result = mysqli_query($this->connection, $query) or die("Ошибка запроса " . mysqli_error($this->connection));

        if($result && mysqli_num_rows($result) > 0) {
            foreach ($result as $key => $value) {
                $commentariesModel->set($key, $value);
            }
        }

        $this->close();
        return $commentariesModel;
    }

    /**
     * @return Model
     */
    public function getAllPosts() {

        $postsModel = new Model();

        $this->connect();

        $query = "SELECT * FROM posts;";
        $result = mysqli_query($this->connection, $query) or die("Ошибка запроса " . mysqli_error($this->connection));

        if($result && mysqli_num_rows($result) > 0) {

            foreach ($result as $key => $value) {

                $postsModel->set($key, $value);

            }
        }

        return $postsModel;
    }

}