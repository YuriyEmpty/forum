<?php
/**
 * Create by: Yuriy Empty
 * Date: 02.03.2019
 * Time: 21:24
 */

namespace Core\Database;



class Connection {
    private $config = null;
    protected $connection = null;

    public function __construct() {
           $this->config = require $_SERVER["DOCUMENT_ROOT"]."/Core/Database/config.php";

    }

    protected function connect() {
        $this->connection = mysqli_connect(
            $this->config["host"],
            $this->config["user"],
            $this->config["password"],
            $this->config["database"],
            $this->config["port"]) or die("Ошибка соединения " . mysqli_error($this->connection));
    }

    protected function close() {
        mysqli_close($this->connection);
    }

}

