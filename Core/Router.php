<?php
/**
 * Create by: Yuriy Empty
 * Date: 02.03.2019
 * Time: 13:59
 */

namespace Core;

use Klein\Klein;

class Router {

    private $klein = null;

    public function __construct() {
        $this->klein = new Klein();

        $this->initRoutes();
    }

    private function initRoutes() {
        session_start();

        $this->klein->respond("GET", "/", function() {
            return $this->includeBaseTemplate("index");
        });

        $this->klein->respond("GET", "/login", function() {
            return $this->includeBaseTemplate("login");
        });

        $this->klein->respond("GET", "/registration", function() {
            return $this->includeBaseTemplate("registration");
        });

        $this->klein->respond("GET", "/post", function() {
            header("Location: / ");
            exit();
        });

        $this->klein->respond("GET", "/post/[:id]", function() {
            return $this->includeBaseTemplate("post");
        });

        $this->klein->respond("GET", "/create-post", function() {
            return $this->includeBaseTemplate("createPost");
        });

    }

    private function includeBaseTemplate($templateName) {

        $controllerPath = "\\Core\\Controllers\\".ucfirst($templateName)."Controller";

        $controller = new $controllerPath();
        $variables = $controller->show();
        $variables["template"] = $templateName.".php";

        extract($variables);
        include_once $_SERVER["DOCUMENT_ROOT"]."/templates/layouts/template.php";
    }

    public function dispatch() {
        $this->klein->dispatch();
    }

}