<?php
/**
 * Create by: Yuriy Empty
 * Date: 02.03.2019
 * Time: 16:24
 */

namespace Core;


abstract class Controller {

    public function __construct() {}

    public abstract function show();

    static public function includeTemplate($templateName, $variables = []) {
        if(!empty($variables)) extract($variables);
        require_once $_SERVER["DOCUMENT_ROOT"]."/templates/".$templateName."";
    }

}