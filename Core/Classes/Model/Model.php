<?php
/**
 * Create by: Yuriy Empty
 * Date: 01.03.2019
 * Time: 22:04
 */

namespace Core\Classes\Model;
use Iterator;

class Model implements Iterator {

    private $array = [];

    public function __construct() {}

    public function set($key, $value) {
        $this->array[$key] = $value;
    }

    public function get($key) {
        return $this->array[$key];
    }

    public function getArray() {
        return $this->array;
    }

    public function delete($key) {
        unset($this->array[$key]);
    }

    public function toString() {
        echo("<pre>");
        print_r($this->array);
        echo("</pre>");
    }

    public function length() {
        return count($this->array);
    }

    /**
     * Return the current element
     * @link http://php.net/manual/en/iterator.current.php
     * @return mixed Can return any type.
     * @since 5.0.0
     */
    public function current() {
        return current($this->array);
    }

    /**
     * Move forward to next element
     * @link http://php.net/manual/en/iterator.next.php
     * @return void Any returned value is ignored.
     * @since 5.0.0
     */
    public function next() {
        next($this->array);
    }

    /**
     * Return the key of the current element
     * @link http://php.net/manual/en/iterator.key.php
     * @return mixed scalar on success, or null on failure.
     * @since 5.0.0
     */
    public function key() {
        return key($this->array);
    }

    /**
     * Checks if current position is valid
     * @link http://php.net/manual/en/iterator.valid.php
     * @return boolean The return value will be casted to boolean and then evaluated.
     * Returns true on success or false on failure.
     * @since 5.0.0
     */
    public function valid() {
        return key($this->array) !== null;
    }

    /**
     * Rewind the Iterator to the first element
     * @link http://php.net/manual/en/iterator.rewind.php
     * @return void Any returned value is ignored.
     * @since 5.0.0
     */
    public function rewind() {
        reset($this->array);
    }

}