<?php
/**
 * Create by: Yuriy Empty
 * Date: 03.03.2019
 * Time: 00:22
 */

namespace Core\Classes;

use Core\Database\RequestsManager\RequestManager;

class User {

    public function __construct() {}

    public function authorize($email, $password) {

        $database = new RequestManager();
        $user = $database->getUserByEmail($email);
        if(!$user || $password != $user["password"]) return false;

        if(!$_SESSION) session_start();
        $_SESSION["USER"] = $user;

        setcookie("authorize", "true",0);

        return true;

    }

    public static function logout() {
        if(!$_SESSION) session_start();
        if(isset($_SESSION["USER"])) {
            unset($_SESSION["USER"]);
            return true;
        }
        return false;
    }

    public static function isAuthorize() {
        if(!$_SESSION) session_start();
        if(isset($_SESSION["USER"])) return true;
        return false;
    }

    public static function getCurrentUser() {
        if(!$_SESSION) session_start();
        if(isset($_SESSION["USER"])) return $_SESSION["USER"];
        return false;
    }

}