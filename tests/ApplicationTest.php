<?php
/**
 * Create by: Yuriy Empty
 * Date: 04.03.2019
 * Time: 00:43
 */

class ApplicationTest extends \PHPUnit\Framework\TestCase {

    public function testCollection() {

        $collection = new \Core\Classes\Collection\Collection();
        $collection->set("value");
        $collection->set("value2");

        $this->assertClassHasAttribute("position", \Core\Classes\Collection\Collection::class);
        $this->assertClassHasAttribute("array", \Core\Classes\Collection\Collection::class);

        $this->assertNotEmpty($collection->getArray());
        $this->assertCount(2, $collection->getArray());
        $this->assertContains("value2", $collection->getArray());
        $this->assertEquals(1, $collection->get("value2"));
    }

    public function testModel() {

        $model = new \Core\Classes\Model\Model();
        $model->set("key", "value");
        $model->set("key2", [1 => "2"]);


        $this->assertClassHasAttribute("array", \Core\Classes\Model\Model::class);
        $this->assertNotEmpty($model->getArray());
        $this->assertNotEmpty($model->get("key"));
        $this->assertEmpty($model->get("dontHaveKey"));
        $this->assertCount(2, $model->getArray());
        $this->assertArrayHasKey("key2", $model->getArray());
        $this->assertArraySubset(["key" => "value"],$model->getArray());
        $this->assertArraySubset(["key2" => [1 => "2"]],$model->getArray());
    }
    
}